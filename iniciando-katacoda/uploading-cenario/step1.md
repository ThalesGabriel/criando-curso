
&nbsp;

## No último passo do cenário `criando-cenario` nós vimos que a continuação era a criação de um webhook, este é um passo a passo simples então caso você não tenha crie uma conta no `bitbucket`. **Ele é ótimo para criação de repositórios privados, já que todos os seus repositórios podem possuir essa característica.**

&nbsp;&nbsp;

# Lembre-se de mantê-lo público!

&nbsp;&nbsp;

![Exemplo](https://bitbucket.org/ThalesGabriel/katacoda-scenarios/src/master/uploading-cenario/exemplo1.png)

&nbsp;&nbsp;

Entre na pasta onde criou o primeiro cenário `criando-cenario` porém lembre-se, a estrutura que você deve manter é

&nbsp;&nbsp;

1. Pasta que contém o cenário `criando-cenario`
    - `criando-cenario`
    - `.git` (Depois do git init esta pasta irá aparecer dependendo apenas se você habilitou a exibição de pastas ocultas)

&nbsp;&nbsp;

> Connect your existing repository to Bitbucket


