&nbsp;&nbsp;

## Volte ao perfil de seu projeto no butbucket e clique em `Repository settings`

&nbsp;&nbsp;

![Exemplo2](https://bitbucket.org/ThalesGabriel/katacoda-scenarios/src/master/uploading-cenario/exemplo2.png)

&nbsp;&nbsp;

Clique em `Webhooks` e `Add webhook`

1. Adicione um título de sua escolha
2. em `URL` adicione https://editor.katacoda.com/scenarios/updated
3. Clique em `save`