## Bônus

<br /><br />

Para aqueles que preferem uma extensão baseada em IDE, Katacoda estendeu o Visual Studio Code para gerenciamento de cenário.

<br /><br />

Isso pode ser instalado a partir da página Katacoda no Visual Studio Marketplace.

<br /><br />

Após a instalação, você pode criar novos cenários, adicionar etapas e ter acesso rápido aos recursos de marcação Katacoda por meio de snippets.